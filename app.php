<?php
/*Simple REST API
* Code here is PHP5.6+
* 1. Download the app to your webserver.
* 2. Test the script like below
* a http://address.tld?what=members to see what is displayed in the browser.
* b http://address.tld to see something else
* c http://address.tld?what=nothing and see comments
*/

//Assume the array of members is the data fetch from a database
$members = [
	[
		'name' => 'David',
		'age' => 35,
		'country' => 'Uganda',
		'Skills' => ['Coding (HTMLX/5,CSS2/3, Vanilla JS, ReactJS, VueJS, PHP5/7)', 'CMS(WordPress)','Soccer'],
		'height' => '5ft'
	],
	[
		'name' => 'Jonnathan',
		'age' => 25,
		'country' => 'Uganda',
		'Skills' => ['Blogging','Mobile Apps Development','Soccer'],
		'height' => '5.11ft'
	],
	[
		'name' => 'Miriam',
		'age' => 27,
		'country' => 'Kenya',
		'Skills' => ['Blogging','Mobile Apps Development','Soccer'],
		'height' => '5.7ft'
	]
];

//This is typecasting php super $_REQUEST super global array to local object
$request = ( object ) $_REQUEST;

//Checking if a what variable was part of the request data sent by the browser to the server
if( isset( $request->what ) )
	if( $request->what == 'members' )
		echo json_encode( $members, JSON_PRETTY_PRINT );
	else
		die( 'Specify correct value for what you are requesting from me.' );

else
	die('Cheatin uh!' );

 ?>
