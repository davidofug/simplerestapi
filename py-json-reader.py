#Reading JSON from URL using python
#First install requests module
#use python -m pip install requests

import requests
url = 'http://localhost/simpleRESTAPI/app.php?what=members' #Replace URL string with yours.
request = requests.get( url )
print( request.json() )
